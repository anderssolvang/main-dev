Rails.application.routes.draw do
  resources :location_data
  resources :economics
  resources :populations
  resources :postcodes
  resources :lower_layer_output_areas
  resources :middle_layer_output_areas
  resources :wards
  resources :district_postcodes
  resources :districts
  resources :countries do
    collection { post :import }
  end

# IKKE I BRUK LENGRE
#  get '/areas/districts/:district', :to => 'areas#district', as: 'areas_districts'
#  get '/areas/wards/:ward', :to => 'areas#ward', as: 'areas_wards'


  resources :addresses
  # FOLLOWS ROUTES
  resources :follow, only: [:create, :destroy]

  # STATIC PAGES
  root 'static_pages#index'

  # USERS ROUTES
  resources :users, only: [:show]
  get "search", :to => "users#search"

  # IMPORT ROUTES
  get "import", :to => "imports#index"
  post "import_countries", :to => "imports#area_import"
  post "import_postcodes", :to => "imports#import"

  # DEVISE RELATED
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout' }

end

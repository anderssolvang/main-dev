require "application_system_test_case"

class LocationDataTest < ApplicationSystemTestCase
  setup do
    @location_datum = location_data(:one)
  end

  test "visiting the index" do
    visit location_data_url
    assert_selector "h1", text: "Location Data"
  end

  test "creating a Location datum" do
    visit location_data_url
    click_on "New Location Datum"

    fill_in "Altitude", with: @location_datum.altitude
    fill_in "Easting", with: @location_datum.easting
    fill_in "Grid ref", with: @location_datum.grid_ref
    fill_in "Latitude", with: @location_datum.latitude
    fill_in "Longitude", with: @location_datum.longitude
    fill_in "Northing", with: @location_datum.northing
    fill_in "Plus code", with: @location_datum.plus_code
    fill_in "Postcode", with: @location_datum.postcode_id
    click_on "Create Location datum"

    assert_text "Location datum was successfully created"
    click_on "Back"
  end

  test "updating a Location datum" do
    visit location_data_url
    click_on "Edit", match: :first

    fill_in "Altitude", with: @location_datum.altitude
    fill_in "Easting", with: @location_datum.easting
    fill_in "Grid ref", with: @location_datum.grid_ref
    fill_in "Latitude", with: @location_datum.latitude
    fill_in "Longitude", with: @location_datum.longitude
    fill_in "Northing", with: @location_datum.northing
    fill_in "Plus code", with: @location_datum.plus_code
    fill_in "Postcode", with: @location_datum.postcode_id
    click_on "Update Location datum"

    assert_text "Location datum was successfully updated"
    click_on "Back"
  end

  test "destroying a Location datum" do
    visit location_data_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Location datum was successfully destroyed"
  end
end

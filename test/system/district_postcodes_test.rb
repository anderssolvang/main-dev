require "application_system_test_case"

class DistrictPostcodesTest < ApplicationSystemTestCase
  setup do
    @district_postcode = district_postcodes(:one)
  end

  test "visiting the index" do
    visit district_postcodes_url
    assert_selector "h1", text: "District Postcodes"
  end

  test "creating a District postcode" do
    visit district_postcodes_url
    click_on "New District Postcode"

    fill_in "District", with: @district_postcode.district_id
    fill_in "District postcode", with: @district_postcode.district_postcode
    click_on "Create District postcode"

    assert_text "District postcode was successfully created"
    click_on "Back"
  end

  test "updating a District postcode" do
    visit district_postcodes_url
    click_on "Edit", match: :first

    fill_in "District", with: @district_postcode.district_id
    fill_in "District postcode", with: @district_postcode.district_postcode
    click_on "Update District postcode"

    assert_text "District postcode was successfully updated"
    click_on "Back"
  end

  test "destroying a District postcode" do
    visit district_postcodes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "District postcode was successfully destroyed"
  end
end

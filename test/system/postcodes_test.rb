require "application_system_test_case"

class PostcodesTest < ApplicationSystemTestCase
  setup do
    @postcode = postcodes(:one)
  end

  test "visiting the index" do
    visit postcodes_url
    assert_selector "h1", text: "Postcodes"
  end

  test "creating a Postcode" do
    visit postcodes_url
    click_on "New Postcode"

    fill_in "Lower layer output area", with: @postcode.lower_layer_output_area_id
    fill_in "Postcode", with: @postcode.postcode
    click_on "Create Postcode"

    assert_text "Postcode was successfully created"
    click_on "Back"
  end

  test "updating a Postcode" do
    visit postcodes_url
    click_on "Edit", match: :first

    fill_in "Lower layer output area", with: @postcode.lower_layer_output_area_id
    fill_in "Postcode", with: @postcode.postcode
    click_on "Update Postcode"

    assert_text "Postcode was successfully updated"
    click_on "Back"
  end

  test "destroying a Postcode" do
    visit postcodes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Postcode was successfully destroyed"
  end
end

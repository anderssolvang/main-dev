require "application_system_test_case"

class EconomicsTest < ApplicationSystemTestCase
  setup do
    @economic = economics(:one)
  end

  test "visiting the index" do
    visit economics_url
    assert_selector "h1", text: "Economics"
  end

  test "creating a Economic" do
    visit economics_url
    click_on "New Economic"

    fill_in "Index of multiple deprivation", with: @economic.index_of_multiple_deprivation
    fill_in "Lower layer output area", with: @economic.lower_layer_output_area_id
    fill_in "Year", with: @economic.year
    click_on "Create Economic"

    assert_text "Economic was successfully created"
    click_on "Back"
  end

  test "updating a Economic" do
    visit economics_url
    click_on "Edit", match: :first

    fill_in "Index of multiple deprivation", with: @economic.index_of_multiple_deprivation
    fill_in "Lower layer output area", with: @economic.lower_layer_output_area_id
    fill_in "Year", with: @economic.year
    click_on "Update Economic"

    assert_text "Economic was successfully updated"
    click_on "Back"
  end

  test "destroying a Economic" do
    visit economics_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Economic was successfully destroyed"
  end
end

require "application_system_test_case"

class MiddleLayerOutputAreasTest < ApplicationSystemTestCase
  setup do
    @middle_layer_output_area = middle_layer_output_areas(:one)
  end

  test "visiting the index" do
    visit middle_layer_output_areas_url
    assert_selector "h1", text: "Middle Layer Output Areas"
  end

  test "creating a Middle layer output area" do
    visit middle_layer_output_areas_url
    click_on "New Middle Layer Output Area"

    fill_in "Middle layer output area", with: @middle_layer_output_area.middle_layer_output_area
    fill_in "Msoa code", with: @middle_layer_output_area.msoa_code
    fill_in "Ward", with: @middle_layer_output_area.ward_id
    click_on "Create Middle layer output area"

    assert_text "Middle layer output area was successfully created"
    click_on "Back"
  end

  test "updating a Middle layer output area" do
    visit middle_layer_output_areas_url
    click_on "Edit", match: :first

    fill_in "Middle layer output area", with: @middle_layer_output_area.middle_layer_output_area
    fill_in "Msoa code", with: @middle_layer_output_area.msoa_code
    fill_in "Ward", with: @middle_layer_output_area.ward_id
    click_on "Update Middle layer output area"

    assert_text "Middle layer output area was successfully updated"
    click_on "Back"
  end

  test "destroying a Middle layer output area" do
    visit middle_layer_output_areas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Middle layer output area was successfully destroyed"
  end
end

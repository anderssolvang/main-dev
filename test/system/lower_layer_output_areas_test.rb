require "application_system_test_case"

class LowerLayerOutputAreasTest < ApplicationSystemTestCase
  setup do
    @lower_layer_output_area = lower_layer_output_areas(:one)
  end

  test "visiting the index" do
    visit lower_layer_output_areas_url
    assert_selector "h1", text: "Lower Layer Output Areas"
  end

  test "creating a Lower layer output area" do
    visit lower_layer_output_areas_url
    click_on "New Lower Layer Output Area"

    fill_in "Lower layer output area", with: @lower_layer_output_area.lower_layer_output_area
    fill_in "Lsoa code", with: @lower_layer_output_area.lsoa_code
    fill_in "Middle layer output area", with: @lower_layer_output_area.middle_layer_output_area_id
    click_on "Create Lower layer output area"

    assert_text "Lower layer output area was successfully created"
    click_on "Back"
  end

  test "updating a Lower layer output area" do
    visit lower_layer_output_areas_url
    click_on "Edit", match: :first

    fill_in "Lower layer output area", with: @lower_layer_output_area.lower_layer_output_area
    fill_in "Lsoa code", with: @lower_layer_output_area.lsoa_code
    fill_in "Middle layer output area", with: @lower_layer_output_area.middle_layer_output_area_id
    click_on "Update Lower layer output area"

    assert_text "Lower layer output area was successfully updated"
    click_on "Back"
  end

  test "destroying a Lower layer output area" do
    visit lower_layer_output_areas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Lower layer output area was successfully destroyed"
  end
end

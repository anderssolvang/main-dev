require 'test_helper'

class LowerLayerOutputAreasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @lower_layer_output_area = lower_layer_output_areas(:one)
  end

  test "should get index" do
    get lower_layer_output_areas_url
    assert_response :success
  end

  test "should get new" do
    get new_lower_layer_output_area_url
    assert_response :success
  end

  test "should create lower_layer_output_area" do
    assert_difference('LowerLayerOutputArea.count') do
      post lower_layer_output_areas_url, params: { lower_layer_output_area: { lower_layer_output_area: @lower_layer_output_area.lower_layer_output_area, lsoa_code: @lower_layer_output_area.lsoa_code, middle_layer_output_area_id: @lower_layer_output_area.middle_layer_output_area_id } }
    end

    assert_redirected_to lower_layer_output_area_url(LowerLayerOutputArea.last)
  end

  test "should show lower_layer_output_area" do
    get lower_layer_output_area_url(@lower_layer_output_area)
    assert_response :success
  end

  test "should get edit" do
    get edit_lower_layer_output_area_url(@lower_layer_output_area)
    assert_response :success
  end

  test "should update lower_layer_output_area" do
    patch lower_layer_output_area_url(@lower_layer_output_area), params: { lower_layer_output_area: { lower_layer_output_area: @lower_layer_output_area.lower_layer_output_area, lsoa_code: @lower_layer_output_area.lsoa_code, middle_layer_output_area_id: @lower_layer_output_area.middle_layer_output_area_id } }
    assert_redirected_to lower_layer_output_area_url(@lower_layer_output_area)
  end

  test "should destroy lower_layer_output_area" do
    assert_difference('LowerLayerOutputArea.count', -1) do
      delete lower_layer_output_area_url(@lower_layer_output_area)
    end

    assert_redirected_to lower_layer_output_areas_url
  end
end

require 'test_helper'

class LocationDataControllerTest < ActionDispatch::IntegrationTest
  setup do
    @location_datum = location_data(:one)
  end

  test "should get index" do
    get location_data_url
    assert_response :success
  end

  test "should get new" do
    get new_location_datum_url
    assert_response :success
  end

  test "should create location_datum" do
    assert_difference('LocationDatum.count') do
      post location_data_url, params: { location_datum: { altitude: @location_datum.altitude, easting: @location_datum.easting, grid_ref: @location_datum.grid_ref, latitude: @location_datum.latitude, longitude: @location_datum.longitude, northing: @location_datum.northing, plus_code: @location_datum.plus_code, postcode_id: @location_datum.postcode_id } }
    end

    assert_redirected_to location_datum_url(LocationDatum.last)
  end

  test "should show location_datum" do
    get location_datum_url(@location_datum)
    assert_response :success
  end

  test "should get edit" do
    get edit_location_datum_url(@location_datum)
    assert_response :success
  end

  test "should update location_datum" do
    patch location_datum_url(@location_datum), params: { location_datum: { altitude: @location_datum.altitude, easting: @location_datum.easting, grid_ref: @location_datum.grid_ref, latitude: @location_datum.latitude, longitude: @location_datum.longitude, northing: @location_datum.northing, plus_code: @location_datum.plus_code, postcode_id: @location_datum.postcode_id } }
    assert_redirected_to location_datum_url(@location_datum)
  end

  test "should destroy location_datum" do
    assert_difference('LocationDatum.count', -1) do
      delete location_datum_url(@location_datum)
    end

    assert_redirected_to location_data_url
  end
end

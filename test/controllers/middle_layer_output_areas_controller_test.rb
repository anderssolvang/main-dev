require 'test_helper'

class MiddleLayerOutputAreasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @middle_layer_output_area = middle_layer_output_areas(:one)
  end

  test "should get index" do
    get middle_layer_output_areas_url
    assert_response :success
  end

  test "should get new" do
    get new_middle_layer_output_area_url
    assert_response :success
  end

  test "should create middle_layer_output_area" do
    assert_difference('MiddleLayerOutputArea.count') do
      post middle_layer_output_areas_url, params: { middle_layer_output_area: { middle_layer_output_area: @middle_layer_output_area.middle_layer_output_area, msoa_code: @middle_layer_output_area.msoa_code, ward_id: @middle_layer_output_area.ward_id } }
    end

    assert_redirected_to middle_layer_output_area_url(MiddleLayerOutputArea.last)
  end

  test "should show middle_layer_output_area" do
    get middle_layer_output_area_url(@middle_layer_output_area)
    assert_response :success
  end

  test "should get edit" do
    get edit_middle_layer_output_area_url(@middle_layer_output_area)
    assert_response :success
  end

  test "should update middle_layer_output_area" do
    patch middle_layer_output_area_url(@middle_layer_output_area), params: { middle_layer_output_area: { middle_layer_output_area: @middle_layer_output_area.middle_layer_output_area, msoa_code: @middle_layer_output_area.msoa_code, ward_id: @middle_layer_output_area.ward_id } }
    assert_redirected_to middle_layer_output_area_url(@middle_layer_output_area)
  end

  test "should destroy middle_layer_output_area" do
    assert_difference('MiddleLayerOutputArea.count', -1) do
      delete middle_layer_output_area_url(@middle_layer_output_area)
    end

    assert_redirected_to middle_layer_output_areas_url
  end
end

require 'test_helper'

class DistrictPostcodesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @district_postcode = district_postcodes(:one)
  end

  test "should get index" do
    get district_postcodes_url
    assert_response :success
  end

  test "should get new" do
    get new_district_postcode_url
    assert_response :success
  end

  test "should create district_postcode" do
    assert_difference('DistrictPostcode.count') do
      post district_postcodes_url, params: { district_postcode: { district_id: @district_postcode.district_id, district_postcode: @district_postcode.district_postcode } }
    end

    assert_redirected_to district_postcode_url(DistrictPostcode.last)
  end

  test "should show district_postcode" do
    get district_postcode_url(@district_postcode)
    assert_response :success
  end

  test "should get edit" do
    get edit_district_postcode_url(@district_postcode)
    assert_response :success
  end

  test "should update district_postcode" do
    patch district_postcode_url(@district_postcode), params: { district_postcode: { district_id: @district_postcode.district_id, district_postcode: @district_postcode.district_postcode } }
    assert_redirected_to district_postcode_url(@district_postcode)
  end

  test "should destroy district_postcode" do
    assert_difference('DistrictPostcode.count', -1) do
      delete district_postcode_url(@district_postcode)
    end

    assert_redirected_to district_postcodes_url
  end
end

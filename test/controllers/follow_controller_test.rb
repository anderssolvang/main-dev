require 'test_helper'

class FollowControllerTest < ActionDispatch::IntegrationTest
  test "should get follows" do
    get follow_follows_url
    assert_response :success
  end

  test "should get followers" do
    get follow_followers_url
    assert_response :success
  end

end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_19_164526) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "fuzzystrmatch"
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.text "subdivision"
    t.text "number"
    t.text "street"
    t.text "locality_address"
    t.text "post_town"
    t.text "county"
    t.text "postcode"
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "country"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "district_postcodes", force: :cascade do |t|
    t.string "district_postcode"
    t.bigint "district_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["district_id"], name: "index_district_postcodes_on_district_id"
  end

  create_table "districts", force: :cascade do |t|
    t.string "district_code"
    t.string "district"
    t.bigint "country_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["country_id"], name: "index_districts_on_country_id"
  end

  create_table "economics", force: :cascade do |t|
    t.bigint "lower_layer_output_area_id", null: false
    t.string "index_of_multiple_deprivation"
    t.string "year"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["lower_layer_output_area_id"], name: "index_economics_on_lower_layer_output_area_id"
  end

  create_table "follows", force: :cascade do |t|
    t.integer "follower_id"
    t.integer "followed_user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "location_data", force: :cascade do |t|
    t.bigint "postcode_id", null: false
    t.string "latitude"
    t.string "longitude"
    t.string "altitude"
    t.string "easting"
    t.string "northing"
    t.string "grid_ref"
    t.string "plus_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["postcode_id"], name: "index_location_data_on_postcode_id"
  end

  create_table "lower_layer_output_areas", force: :cascade do |t|
    t.string "lower_layer_output_area"
    t.string "lsoa_code"
    t.bigint "middle_layer_output_area_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["middle_layer_output_area_id"], name: "index_lower_layer_output_areas_on_middle_layer_output_area_id"
  end

  create_table "middle_layer_output_areas", force: :cascade do |t|
    t.string "middle_layer_output_area"
    t.string "msoa_code"
    t.bigint "ward_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ward_id"], name: "index_middle_layer_output_areas_on_ward_id"
  end

  create_table "populations", force: :cascade do |t|
    t.bigint "ward_id", null: false
    t.string "poplation_type"
    t.string "population"
    t.string "year"
    t.string "age"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["ward_id"], name: "index_populations_on_ward_id"
  end

  create_table "postcodes", force: :cascade do |t|
    t.bigint "lower_layer_output_area_id", null: false
    t.string "postcode"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "district_postcode_id"
    t.index ["district_postcode_id"], name: "index_postcodes_on_district_postcode_id"
    t.index ["lower_layer_output_area_id"], name: "index_postcodes_on_lower_layer_output_area_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "admin", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "wards", force: :cascade do |t|
    t.string "ward"
    t.string "ward_code"
    t.bigint "district_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["district_id"], name: "index_wards_on_district_id"
  end

  add_foreign_key "district_postcodes", "districts"
  add_foreign_key "districts", "countries"
  add_foreign_key "economics", "lower_layer_output_areas"
  add_foreign_key "location_data", "postcodes"
  add_foreign_key "lower_layer_output_areas", "middle_layer_output_areas"
  add_foreign_key "middle_layer_output_areas", "wards"
  add_foreign_key "populations", "wards"
  add_foreign_key "postcodes", "district_postcodes"
  add_foreign_key "postcodes", "lower_layer_output_areas"
  add_foreign_key "wards", "districts"
end

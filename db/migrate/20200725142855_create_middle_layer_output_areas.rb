class CreateMiddleLayerOutputAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :middle_layer_output_areas do |t|
      t.string :middle_layer_output_area
      t.string :msoa_code
      t.references :ward, null: false, foreign_key: true

      t.timestamps
    end
  end
end

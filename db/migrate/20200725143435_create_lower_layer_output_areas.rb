class CreateLowerLayerOutputAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :lower_layer_output_areas do |t|
      t.string :lower_layer_output_area
      t.string :lsoa_code
      t.references :middle_layer_output_area, null: false, foreign_key: true

      t.timestamps
    end
  end
end

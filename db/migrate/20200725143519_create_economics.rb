class CreateEconomics < ActiveRecord::Migration[6.0]
  def change
    create_table :economics do |t|
      t.references :lower_layer_output_area, null: false, foreign_key: true
      t.string :index_of_multiple_deprivation
      t.string :year

      t.timestamps
    end
  end
end

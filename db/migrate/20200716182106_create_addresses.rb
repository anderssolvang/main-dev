class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.text :subdivision
      t.text :number
      t.text :street
      t.text :locality_address
      t.text :post_town
      t.text :county
      t.text :postcode
      t.references :addressable, polymorphic: true

      t.timestamps
    end
  end
end

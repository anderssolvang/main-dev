class CreatePostcodes < ActiveRecord::Migration[6.0]
  def change
    create_table :postcodes do |t|
      t.references :lower_layer_output_area, null: false, foreign_key: true
      t.string :postcode

      t.timestamps
    end
  end
end

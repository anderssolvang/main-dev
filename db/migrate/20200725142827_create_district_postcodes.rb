class CreateDistrictPostcodes < ActiveRecord::Migration[6.0]
  def change
    create_table :district_postcodes do |t|
      t.string :district_postcode
      t.references :district, null: false, foreign_key: true

      t.timestamps
    end
  end
end

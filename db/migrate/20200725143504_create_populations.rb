class CreatePopulations < ActiveRecord::Migration[6.0]
  def change
    create_table :populations do |t|
      t.references :ward, null: false, foreign_key: true
      t.string :poplation_type
      t.string :population
      t.string :year
      t.string :age

      t.timestamps
    end
  end
end

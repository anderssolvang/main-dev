class CreateLocationData < ActiveRecord::Migration[6.0]
  def change
    create_table :location_data do |t|
      t.references :postcode, null: false, foreign_key: true
      t.string :latitude
      t.string :longitude
      t.string :altitude
      t.string :easting
      t.string :northing
      t.string :grid_ref
      t.string :plus_code

      t.timestamps
    end
  end
end

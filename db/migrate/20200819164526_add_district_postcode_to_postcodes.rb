class AddDistrictPostcodeToPostcodes < ActiveRecord::Migration[6.0]
  def change
    add_reference :postcodes, :district_postcode, foreign_key: true, index: true
  end
end

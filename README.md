# README

The Aviny application aim to become a tool for research of new property developments and investments, as well as managing properties on the fly. The application is aimed at the UK market, but is being built in a way that allows dublication to other countries.

Features that we intend to build:

A wall
 - A following feature
 - A messageing system between users
 - Send status updates, ask for advice, discuss, ect...
 - Based on people you follow, areas you follow, ect...
Area research
- Semi automated
  - searchable based on criterias to the investor
  - other users in the area
  - services available in the area (Property management, builders, ect)
Property research
- do standard research automated, or semi automated
- find good potential properties
   - messaging owners/realtors
- builders to do renovation quotes
- surveyor leads
- loan applications simplified
  - bridge
  - standard LTD
- solicitors to do searches
Property management
- messaging between stakeholders
- semi automated maintenance scheduler
- rent payments and follow up
- basic shop for standard items (lightbulps, ect...)
- electronic locking system based on payments

Investor opportunities
- Crowd funding type in the end, by issueing stocks
- Search and find investments and JV opportunities



VERSIONS of COMPONENTS

Ruby version: '2.7.0'
Rails: 6.0.3.2
Webpack: 4.43.0
JQuery: 3.4.1 -  (not latest since there is a bug in Bootstrap that prevents using Jquery later versions. The bug is with
                collapable and dropdowns javascript files in the Bootstrap library.)
Stimulus: 1.1.0
Bootstrap: 4.5
Fontawesome: 5.13.1 free version
Postgres db for dev, test and production

Tests not extensivly created yet.

class Follow < ApplicationRecord
  # Search for users
  #include PgSearch
  #pg_search_scope :search_by_full_name, against: [:first_name, :last_name]

  # The user giving the follow
  belongs_to :follower, foreign_key: :follower_id, class_name: "User"

  # The user being followed
  belongs_to :followed_user, foreign_key: :followed_user_id, class_name: "User"
end

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

# PG search for model
include PgSearch::Model
#pg_search_scope :search_by_full_name, against: [:first_name, :last_name],
#                  using: [:tsearch, :trigram]



# FOLLOW FEATURE
  # Returns array of follows for given user
  has_many :received_follows, foreign_key: :followed_user_id, class_name: "Follow"

  # Returns array of users who follow given user
  has_many :followers, through: :received_follows, source: :follower

  # returns array of follows a user has to other users
  has_many :given_follows, foreign_key: :follower_id, class_name: "Follow"

  # returns array of other users who the user has followed.
  has_many :followings, through: :given_follows, source: :followed_user

end

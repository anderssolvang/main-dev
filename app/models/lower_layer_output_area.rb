class LowerLayerOutputArea < ApplicationRecord
  belongs_to :middle_layer_output_area
  has_many :economics
  has_many :postcodes
end

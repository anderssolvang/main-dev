class Postcode < ApplicationRecord
  belongs_to :lower_layer_output_area
  belongs_to :district_postcode
  has_many :location_datum

end

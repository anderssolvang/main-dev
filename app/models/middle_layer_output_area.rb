class MiddleLayerOutputArea < ApplicationRecord
  belongs_to :ward
  has_many :lower_layer_output_areas
end

class DistrictPostcode < ApplicationRecord
  belongs_to :district
  has_many :postcodes
end

class District < ApplicationRecord
  belongs_to :country
  has_many :district_postcodes, -> { order(:district_postcode => :asc) }
  has_many :wards, -> { order(:ward => :asc) }
end

class Country < ApplicationRecord
  has_many :districts, -> { order(:district => :asc) }
end

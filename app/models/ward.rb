class Ward < ApplicationRecord
  belongs_to :district
  has_many :middle_layer_output_areas
  has_many :populations
end

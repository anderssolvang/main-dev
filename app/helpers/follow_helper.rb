module FollowHelper

  def current_user_following (y)
    x = current_user.id
    f = Follow.where(follower_id: x, followed_user_id: y).or(Follow.where(follower_id: y, followed_user_id: x))
    if f.empty?
      false
    else
      true
    end
  end

end

module ApplicationHelper


  # SVG support, to use surrond in any tag with class of color wanted. Then: <%= svg 'name_of_svg' %>
  def svg(name)
    file_path = "#{Rails.root}/app/assets/images/svg/#{name}.svg"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end

  # FLASH message helper
  def flash_type_helper(type, msg)
    if type == "alert"
      flash_message = "<div class='alert alert-warning fade-away'>#{msg}</div>"
    elsif type == "notice"
      flash_message = "<div class='alert alert-success fade-away'>#{msg}</div>"
    elsif type == "error"
      flash_message = "<div class='alert alert-danger fade-away'>#{msg}</div>"
    end
    flash_message.html_safe
  end

  # First name helper to find out if the name is to long
  def first_name_welcome_helper
    if !current_user || current_user.first_name == nil || current_user.first_name.size > 6
      "Welcome"
    else
      "Welcome #{current_user.first_name.capitalize}"
    end
  end
end

class LowerLayerOutputAreasController < ApplicationController
  before_action :set_lower_layer_output_area, only: [:show, :edit, :update, :destroy]

  # GET /lower_layer_output_areas
  # GET /lower_layer_output_areas.json
  def index
    @lower_layer_output_areas = LowerLayerOutputArea.all
  end

  # GET /lower_layer_output_areas/1
  # GET /lower_layer_output_areas/1.json
  def show
    @lloa = LowerLayerOutputArea.find_by_id(params[:id])
    @pcs = Postcode.where(lower_layer_output_area_id: params[:id])

    @economics = Economic.where(lower_layer_output_area_id: @lower_layer_output_area.id)
  end

  # GET /lower_layer_output_areas/new
  def new
    @lower_layer_output_area = LowerLayerOutputArea.new
  end

  # GET /lower_layer_output_areas/1/edit
  def edit
  end

  # POST /lower_layer_output_areas
  # POST /lower_layer_output_areas.json
  def create
    @lower_layer_output_area = LowerLayerOutputArea.new(lower_layer_output_area_params)

    respond_to do |format|
      if @lower_layer_output_area.save
        format.html { redirect_to @lower_layer_output_area, notice: 'Lower layer output area was successfully created.' }
        format.json { render :show, status: :created, location: @lower_layer_output_area }
      else
        format.html { render :new }
        format.json { render json: @lower_layer_output_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lower_layer_output_areas/1
  # PATCH/PUT /lower_layer_output_areas/1.json
  def update
    respond_to do |format|
      if @lower_layer_output_area.update(lower_layer_output_area_params)
        format.html { redirect_to @lower_layer_output_area, notice: 'Lower layer output area was successfully updated.' }
        format.json { render :show, status: :ok, location: @lower_layer_output_area }
      else
        format.html { render :edit }
        format.json { render json: @lower_layer_output_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lower_layer_output_areas/1
  # DELETE /lower_layer_output_areas/1.json
  def destroy
    @lower_layer_output_area.destroy
    respond_to do |format|
      format.html { redirect_to lower_layer_output_areas_url, notice: 'Lower layer output area was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lower_layer_output_area
      @lower_layer_output_area = LowerLayerOutputArea.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def lower_layer_output_area_params
      params.require(:lower_layer_output_area).permit(:lower_layer_output_area, :lsoa_code, :middle_layer_output_area_id)
    end
end

class MiddleLayerOutputAreasController < ApplicationController
  before_action :set_middle_layer_output_area, only: [:show, :edit, :update, :destroy]

  # GET /middle_layer_output_areas
  # GET /middle_layer_output_areas.json
  def index
    @middle_layer_output_areas = MiddleLayerOutputArea.all
  end

  # GET /middle_layer_output_areas/1
  # GET /middle_layer_output_areas/1.json
  def show
    @mloa = MiddleLayerOutputArea.find_by_id(params[:id])
    @lloas = LowerLayerOutputArea.where(middle_layer_output_area_id: params[:id])
  end

  # GET /middle_layer_output_areas/new
  def new
    @middle_layer_output_area = MiddleLayerOutputArea.new
  end

  # GET /middle_layer_output_areas/1/edit
  def edit
  end

  # POST /middle_layer_output_areas
  # POST /middle_layer_output_areas.json
  def create
    @middle_layer_output_area = MiddleLayerOutputArea.new(middle_layer_output_area_params)

    respond_to do |format|
      if @middle_layer_output_area.save
        format.html { redirect_to @middle_layer_output_area, notice: 'Middle layer output area was successfully created.' }
        format.json { render :show, status: :created, location: @middle_layer_output_area }
      else
        format.html { render :new }
        format.json { render json: @middle_layer_output_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /middle_layer_output_areas/1
  # PATCH/PUT /middle_layer_output_areas/1.json
  def update
    respond_to do |format|
      if @middle_layer_output_area.update(middle_layer_output_area_params)
        format.html { redirect_to @middle_layer_output_area, notice: 'Middle layer output area was successfully updated.' }
        format.json { render :show, status: :ok, location: @middle_layer_output_area }
      else
        format.html { render :edit }
        format.json { render json: @middle_layer_output_area.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /middle_layer_output_areas/1
  # DELETE /middle_layer_output_areas/1.json
  def destroy
    @middle_layer_output_area.destroy
    respond_to do |format|
      format.html { redirect_to middle_layer_output_areas_url, notice: 'Middle layer output area was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_middle_layer_output_area
      @middle_layer_output_area = MiddleLayerOutputArea.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def middle_layer_output_area_params
      params.require(:middle_layer_output_area).permit(:middle_layer_output_area, :msoa_code, :ward_id)
    end
end

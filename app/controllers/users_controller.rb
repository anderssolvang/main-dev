class UsersController < ApplicationController
  before_action :authenticate_user!

  def show
    if params[:id] =~ /\A[-+]?[0-9]*\.?[0-9]+\Z/
      @user = User.find(params[:id])
    else
      @user = User.find(current_user.id)
    end
    @follows = @user.followings
    @followers = @user.followers
    @users = search_users
  end

  private

    def search_users

      return User.where("first_name LIKE :search OR last_name LIKE :search", search: "%#{params[:term]}%")

    end
end

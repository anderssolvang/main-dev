class DistrictPostcodesController < ApplicationController
  before_action :set_district_postcode, only: [:show, :edit, :update, :destroy]

  # GET /district_postcodes
  # GET /district_postcodes.json
  def index
    @district_postcodes = DistrictPostcode.all
  end

  # GET /district_postcodes/1
  # GET /district_postcodes/1.json
  def show
  end

  # GET /district_postcodes/new
  def new
    @district_postcode = DistrictPostcode.new
  end

  # GET /district_postcodes/1/edit
  def edit
  end

  # POST /district_postcodes
  # POST /district_postcodes.json
  def create
    @district_postcode = DistrictPostcode.new(district_postcode_params)

    respond_to do |format|
      if @district_postcode.save
        format.html { redirect_to @district_postcode, notice: 'District postcode was successfully created.' }
        format.json { render :show, status: :created, location: @district_postcode }
      else
        format.html { render :new }
        format.json { render json: @district_postcode.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /district_postcodes/1
  # PATCH/PUT /district_postcodes/1.json
  def update
    respond_to do |format|
      if @district_postcode.update(district_postcode_params)
        format.html { redirect_to @district_postcode, notice: 'District postcode was successfully updated.' }
        format.json { render :show, status: :ok, location: @district_postcode }
      else
        format.html { render :edit }
        format.json { render json: @district_postcode.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /district_postcodes/1
  # DELETE /district_postcodes/1.json
  def destroy
    @district_postcode.destroy
    respond_to do |format|
      format.html { redirect_to district_postcodes_url, notice: 'District postcode was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_district_postcode
      @district_postcode = DistrictPostcode.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def district_postcode_params
      params.require(:district_postcode).permit(:district_postcode, :district_id)
    end
end

class FollowController < ApplicationController

  def create
    u = current_user.id
    if check_relationship_exists(u) == true
      follow = Follow.create(follower_id: u, followed_user_id: params[:id])
      if follow.save
        flash[:notice] = "You are now following #{User.find_by_id(params[:id]).first_name.titleize}"
        redirect_to user_path(params[:id])
      else
        flash[:alert] = "Sorry, for some reason you are not able to follow this person. Please notify us and record the time now."
        redirect_to user_path(params[:id])
      end
    else
      flash[:alert] = "You are already following this person"
      redirect_to user_path(params[:id])
    end
  end

  def destroy
    Follow.where(follower_id: current_user.id, followed_user_id: params[:id]).destroy_all
    flash[:notice] = "You are no longer following this person"
    redirect_to user_path(params[:id])
  end


  private

    def check_relationship_exists(u)
      return Follow.where(follower_id: u, followed_user_id: params[:person_id]).empty?
    end
end

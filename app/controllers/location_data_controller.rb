class LocationDataController < ApplicationController
  before_action :set_location_datum, only: [:show, :edit, :update, :destroy]

  # GET /location_data
  # GET /location_data.json
  def index
    @location_data = LocationDatum.all
  end

  # GET /location_data/1
  # GET /location_data/1.json
  def show
  end

  # GET /location_data/new
  def new
    @location_datum = LocationDatum.new
  end

  # GET /location_data/1/edit
  def edit
  end

  # POST /location_data
  # POST /location_data.json
  def create
    @location_datum = LocationDatum.new(location_datum_params)

    respond_to do |format|
      if @location_datum.save
        format.html { redirect_to @location_datum, notice: 'Location datum was successfully created.' }
        format.json { render :show, status: :created, location: @location_datum }
      else
        format.html { render :new }
        format.json { render json: @location_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /location_data/1
  # PATCH/PUT /location_data/1.json
  def update
    respond_to do |format|
      if @location_datum.update(location_datum_params)
        format.html { redirect_to @location_datum, notice: 'Location datum was successfully updated.' }
        format.json { render :show, status: :ok, location: @location_datum }
      else
        format.html { render :edit }
        format.json { render json: @location_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /location_data/1
  # DELETE /location_data/1.json
  def destroy
    @location_datum.destroy
    respond_to do |format|
      format.html { redirect_to location_data_url, notice: 'Location datum was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location_datum
      @location_datum = LocationDatum.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def location_datum_params
      params.require(:location_datum).permit(:postcode_id, :latitude, :longitude, :altitude, :easting, :northing, :grid_ref, :plus_code)
    end
end

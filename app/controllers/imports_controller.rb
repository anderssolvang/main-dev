class ImportsController < ApplicationController

  def index

  end

  def postcode_import(file)
    counter = 1
    tot_lines = IO.readlines(file).size
    # CSV file to be imported
    CSV.foreach(file, {col_sep: ",", headers: true}) do |row|

      if row[1] == 'Yes' && row[12] && row[8] != "" && row[8] != "" || row[8].nil?
        # POSTCODE - Check if record exists, if not then create it
        if record_exists(row[0], "postcode")
          postcode = Postcode.find_by_postcode(row[0])
        else
          dpc = row[0][0..3]
          lloa = LowerLayerOutputArea.find_by_lsoa_code(row[28])
          district_postcode = DistrictPostcode.find_by_district_postcode(dpc)
          if district_postcode.nil?
            district_postcode = DistrictPostcode.find_by_district_postcode(dpc[0..2])
          end
          if district_postcode.nil?
            district_postcode = DistrictPostcode.find_by_district_postcode(dpc[0..1])
          end
          if lloa.id.nil?
            postcode = Postcode.create(lower_layer_output_area_id: 0, district_postcode_id: district_postcode.id, postcode: row[0])
          else
            postcode = Postcode.create(lower_layer_output_area_id: lloa.id, district_postcode_id: district_postcode.id, postcode: row[0])
          end
        end
      end
      counter += 1

      puts "Line #{counter} / #{tot_lines} beeing processed."

    end
  end

  def area_import(file)
    # CSV file to be imported
    CSV.foreach(file, { col_sep: ",", headers: true }) do |row|
      # If In Use? is yes and Country, and ward is not empty, continue on this row
      if row[1] == 'Yes' && row[12] && row[8] != "" && row[8] != "" || row[8].nil?

        # COUNTRY - Check if record exists, if not then create it
        if record_exists(row[12], "country")
          country = Country.find_by_country(row[12])
        else
          country = Country.create(country: row[12])
        end
        country_id =
        # DISTRICT - Check if record exists, if not then create it
        if record_exists(row[8], "district")
          district = District.find_by_district(row[8])
        else
          district = District.create(country_id: country.id, district: row[8], district_code: row[10])
        end

        # DISTRICT POSTCODES - Check if record exists, if not then create it
        unless record_exists(row[42], "district_postcode")
          DistrictPostcode.create(district_id: district.id, district_postcode: row[42])
        end

        # WARD - Check if record exists, if not then create it
        if record_exists(row[9], "ward")
          ward = Ward.find_by_ward(row[9])
        else
          ward = Ward.create(district_id: district.id, ward: row[9], ward_code: row[11])
        end

        # MSOA - Check if record exists, if not then create it
        if record_exists(row[30], "msoa")
          msoa = MiddleLayerOutputArea.find_by_msoa_code(row[30])
        else
          msoa = MiddleLayerOutputArea.create(ward_id: ward.id, middle_layer_output_area: row[31], msoa_code: row[30])
        end

        # LSOA - Check if record exists, if not then create it
        if record_exists(row[28], "lsoa")
          lsoa = LowerLayerOutputArea.find_by_lsoa_code(row[28])
        else
          lsoa = LowerLayerOutputArea.create(middle_layer_output_area_id: msoa.id, lower_layer_output_area: row[23], lsoa_code: row[28])
        end

        # POSTCODE - Check if record exists, if not then create it
        if record_exists(row[0], "postcode")
          postcode = Postcode.find_by_postcode(row[0])
        else
          postcode = Postcode.create(lower_layer_output_area_id: lsoa.id, postcode: row[0])
        end

        # ECONOMICS - Check if record exists, if not then create it
        unless record_exists(lsoa.id, "economics")
          Economic.create(lower_layer_output_area_id: lsoa.id, index_of_multiple_deprivation: row[35], year: "2020")
        end

        # Location_data - Check if record exists, if not then create it
        unless record_exists(postcode.id, "location_data")
          LocationDatum.create(postcode_id: postcode.id, latitude: row[2], longitude: row[3], altitude: row[26], easting: row[4],
                                northing: row[5], grid_ref: row[6], plus_code: row[45])
        end
      end
    end
  end


  def record_exists(record, model)
    case model
    when "country"
      return Country.where(country: record).distinct.select(:country).exists?
    when "district"
      return District.where(district: record).distinct.select(:district).exists?
    when "district_postcode"
      return DistrictPostcode.where(district_postcode: record).distinct.select(:district_postcode).exists?
    when "ward"
      return Ward.where(ward: record).distinct.select(:ward).exists?
    when "msoa"
      return MiddleLayerOutputArea.where(msoa_code: record).distinct.select(:msoa_code).exists?
    when "lsoa"
      return LowerLayerOutputArea.where(lsoa_code: record).distinct.select(:lsoa_code).exists?
    when "postcode"
      return Postcode.where(postcode: record).distinct.select(:postcode).exists?
    when "economics"
      return Economic.where(lower_layer_output_area_id: record).distinct.select(:lower_layer_output_area_id).exists?
    when "location_data"
      return LocationDatum.where(postcode_id: record).distinct.select(:postcode_id).exists?
    else
      puts "This line cannot be processed - def record_exists called"
    end
  end


  def import
    if params[:file].nil?
      redirect_to import_path, alert: "You need to include a file"
    else
      postcode_import(params[:file])
      redirect_to import_path
    end

  end
end

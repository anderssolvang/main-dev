json.extract! economic, :id, :lower_layer_output_area_id, :index_of_multiple_deprivation, :year, :created_at, :updated_at
json.url economic_url(economic, format: :json)

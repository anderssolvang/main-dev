json.extract! district, :id, :district_code, :district, :country_id, :created_at, :updated_at
json.url district_url(district, format: :json)

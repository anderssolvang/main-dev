json.extract! middle_layer_output_area, :id, :middle_layer_output_area, :msoa_code, :ward_id, :created_at, :updated_at
json.url middle_layer_output_area_url(middle_layer_output_area, format: :json)

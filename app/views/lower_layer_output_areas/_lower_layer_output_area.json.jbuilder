json.extract! lower_layer_output_area, :id, :lower_layer_output_area, :lsoa_code, :middle_layer_output_area_id, :created_at, :updated_at
json.url lower_layer_output_area_url(lower_layer_output_area, format: :json)

json.extract! location_datum, :id, :postcode_id, :latitude, :longitude, :altitude, :easting, :northing, :grid_ref, :plus_code, :created_at, :updated_at
json.url location_datum_url(location_datum, format: :json)

json.extract! postcode, :id, :lower_layer_output_area_id, :postcode, :created_at, :updated_at
json.url postcode_url(postcode, format: :json)

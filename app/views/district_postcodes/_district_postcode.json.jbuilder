json.extract! district_postcode, :id, :district_postcode, :district_id, :created_at, :updated_at
json.url district_postcode_url(district_postcode, format: :json)

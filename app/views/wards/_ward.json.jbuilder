json.extract! ward, :id, :ward, :ward_code, :district_id, :created_at, :updated_at
json.url ward_url(ward, format: :json)

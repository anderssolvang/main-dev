
[1mFrom:[0m /mnt/c/code/rails/aviny/app/controllers/areas_controller.rb:104 AreasController#area_import:

    [1;34m102[0m: [32mdef[0m [1;34marea_import[0m(file)
    [1;34m103[0m:   [1;34m# CSV file to be imported[0m
 => [1;34m104[0m:   [1;34;4mCSV[0m.foreach(file, { [35mcol_sep[0m: [31m[1;31m"[0m[31m,[1;31m"[0m[31m[0m, [35mheaders[0m: [1;36mtrue[0m }) [32mdo[0m |row|
    [1;34m105[0m:     [1;34m# If In Use? is yes and Country, and ward is not empty, continue on this row[0m
    [1;34m106[0m:     [32mif[0m row[[1;34m1[0m] == [31m[1;31m'[0m[31mYes[1;31m'[0m[31m[0m && !row[[1;34m12[0m] && !row[[1;34m8[0m]
    [1;34m107[0m:       area = [1;34;4mArea[0m.create([35mpostcode[0m: row[[1;34m0[0m], [35mpostcode_area[0m: row[[1;34m41[0m], [35mpostcode_district[0m: split_postcode(row[[1;34m0[0m], [31m[1;31m"[0m[31mdistrict[1;31m"[0m[31m[0m), [35mpostcode_sector[0m: split_postcode(row[[1;34m0[0m], [31m[1;31m"[0m[31msector[1;31m"[0m[31m[0m), [35mpostcode_unit[0m: split_postcode(row[[1;34m0[0m], [31m[1;31m"[0m[31munit[1;31m"[0m[31m[0m),
    [1;34m108[0m:                        [35mcountry[0m: row[[1;34m12[0m], [35mregion[0m: row[[1;34m25[0m], [35mcounty[0m: row[[1;34m7[0m], [35mcounty_code[0m: row[[1;34m13[0m], [35mdistrict[0m: row[[1;34m8[0m], [35mdistrict_code[0m: row[[1;34m10[0m], [35mward[0m: row[[1;34m9[0m],
    [1;34m109[0m:                        [35mward_code[0m: row[[1;34m11[0m])
    [1;34m110[0m:       [1;34;4mDemographic[0m.create([35marea_id[0m: area.id, [35mpopulation[0m: row[[1;34m19[0m], [35mhouseholds[0m: row[[1;34m20[0m], [35mrural_urban[0m: row[[1;34m24[0m], [35mindex_of_multiple_deprivation[0m: row[[1;34m35[0m],
    [1;34m111[0m:                          [35maverage_income[0m: row[[1;34m46[0m], [35myear[0m: [31m[1;31m"[0m[31m2020[1;31m"[0m[31m[0m)
    [1;34m112[0m:       [1;34;4mGeopgraphicRef[0m.create([35marea_id[0m: area.id, [35mlatitude[0m: row[[1;34m2[0m], [35mlongitude[0m: row[[1;34m3[0m], [35maltitude[0m: row[[1;34m26[0m], [35measting[0m: row[[1;34m4[0m],
    [1;34m113[0m:                              [35mnorthing[0m: row[[1;34m5[0m], [35mgrid_ref[0m: row[[1;34m6[0m], [35mplus_code[0m: row[[1;34m45[0m])
    [1;34m114[0m:       [1;34;4mGovernanceInfo[0m.create([35marea_id[0m: area.id, [35mbuilt_up_area[0m: row[[1;34m21[0m], [35mbuilt_up_sub_division[0m: row[[1;34m22[0m], [35mlondon_zone[0m: row[[1;34m27[0m],
    [1;34m115[0m:                              [35mlsoa_code[0m: row[[1;34m28[0m], [35mlocal_authority[0m: row[[1;34m29[0m], [35mmsoa_code[0m: row[[1;34m30[0m],
    [1;34m116[0m:                              [35mmiddle_layer_super_output_area[0m: row[[1;34m31[0m], [35mlower_layer_super_output_area[0m: row[[1;34m23[0m], [35mcensus_output_area[0m: row[[1;34m33[0m],
    [1;34m117[0m:                              [35mconstituency[0m: row[[1;34m14[0m], [35mconstituency_code[0m: row[[1;34m34[0m], [35mparish[0m: row[[1;34m17[0m], [35mparish_code[0m: row[[1;34m32[0m])
    [1;34m118[0m:       [1;34m# Train stations[0m
    [1;34m119[0m:       [1;34;4mAreaFacility[0m.create([35marea_id[0m: area.id, [35mname[0m: row[[1;34m39[0m], [35mworkers_visitors[0m: row[[1;34m40[0m], [35mtype_of_facility[0m: [31m[1;31m"[0m[31mtransport[1;31m"[0m[31m[0m, [35msub_type[0m: [31m[1;31m"[0m[31mtrain station[1;31m"[0m[31m[0m)
    [1;34m120[0m: 
    [1;34m121[0m:       [1;34m# Police stations[0m
    [1;34m122[0m:       [1;34;4mAreaFacility[0m.create([35marea_id[0m: area.id, [35mname[0m: row[[1;34m43[0m], [35mtype_of_facility[0m: [31m[1;31m"[0m[31mpublic service[1;31m"[0m[31m[0m, [35msub_type[0m: [31m[1;31m"[0m[31mpolice[1;31m"[0m[31m[0m)
    [1;34m123[0m: 
    [1;34m124[0m:       [1;34m# Water company[0m
    [1;34m125[0m:       [1;34;4mAreaFacility[0m.create([35marea_id[0m: area.id, [35mname[0m: row[[1;34m47[0m], [35mtype_of_facility[0m: [31m[1;31m"[0m[31mpublic service[1;31m"[0m[31m[0m, [35msub_type[0m: [31m[1;31m"[0m[31mwater[1;31m"[0m[31m[0m)
    [1;34m126[0m:     [32mend[0m
    [1;34m127[0m:   [32mend[0m
    [1;34m128[0m: [32mend[0m

